(function  ( $, window, document, undefined ) {

  "use strict";

  var Counter, config;

  config = {
    handler: '.js-counter',
    displaySelector: '.js-counter-display',
    resetSelector: '.js-counter-reset',
    restartSelector: '.js-counter-restart',
    activeClass: 'is-active',
    delay: 1000,
    currentValue: 0,
    threshold: null,
    incrementFunc: null
  };

  var Counter = {
    init: function() {
      this.inputValue();
      this.uiHandlers();
    },

    inputValue: function  () {
      var self = this;

      $(config.handler).on("keyup", function(e) {

        config.threshold = $(this).val(); 
        self.restartCounter();   
      });
    },

    resetValues: function  () {
      config.threshold = 0;
      config.currentValue = 0;
      
      $( config.displaySelector ).text( config.currentValue );
    },

    checkValue: function  () {
      // Check if value is blank
      return !$(config.selector).val() ? false : true;
    },

    setIncrement: function  () {
      var self = this,
          val = 0;

      config.incrementFunc = setInterval( function(){        
        $( config.displaySelector ).html(++val); 

        config.currentValue = val;    

        $( config.displaySelector ).trigger("valueChange"); 

        // Toggle classes dependending on divisor
        self.checkDivisible();
      }, config.delay);
    },

    startCounter: function  () {
      config.currentValue = 0;

      this.setIncrement();
    },

    restartCounter: function  () {
      if ( this.checkValue ) {
        $(config.displaySelector).text('0');
        
        this.clearCounter();
        this.startCounter();
      }
    },

    clearCounter: function  () {
      clearInterval( config.incrementFunc );
    },

    checkDivisible: function  () {
      var str = 'data-divisible',
          el = $("[" + str + "]");

      // Check Divisibility
      el.each(function() {
        var divisor = $(this).attr(str);

        $(this).toggleClass( config.activeClass, config.currentValue !== null && (config.currentValue % divisor == 0));
      });
    },

    uiHandlers: function() {
      var self = this;

      // Reset
      $(config.resetSelector).on("click", function  () {
        $( config.handler ).val('');

        self.resetValues();
        self.clearCounter();
      });

      // Restart
      $(config.restartSelector).on("click", function  () {

        if ( config.currentValue !== 0 ) {
          self.restartCounter();
        }
      });

      // Custom Listener
      $( config.displaySelector ).on("valueChange", function  () {
        
        if ( config.currentValue >= config.threshold ) {
          self.clearCounter();
        }
      });
    }    
  };

  Counter.init();

} )( jQuery, window, document );