const gulpRequireTasks = require('gulp-require-tasks');

var gulp = require('gulp'),
	batch = require('gulp-batch'),
	watch = require('gulp-watch'),
 	plugins = require('gulp-load-plugins')(),
 	taskDir = './gulp-tasks/';

// Load Tasks
gulp.task('uglify', require( taskDir + 'uglify')(gulp, plugins));
gulp.task('less', require( taskDir + 'less')(gulp, plugins));

// Set Tasks
gulp.task('default', ['less', 'uglify']);

gulp.task('js', ['uglify']);
gulp.task('style', ['less']);


gulp.task('watch', function () {
    watch('less/**/*.less', batch(function (events, done) {
        gulp.start('less', done);
    }));

    watch('js/**/*.js', batch(function (events, done) {
        gulp.start('uglify', done);
    }));
});