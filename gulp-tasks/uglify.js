module.exports = function ( gulp, plugins ) {
	
	var gulp = require('gulp'),
	 	gulpIf = require('gulp-if'),
	 	gulpConcat = require('gulp-concat'),
	 	sourcemaps = require('gulp-sourcemaps'),
		proj = require('../gulp-config'),
		uglify = require('gulp-uglify');

	var taskConfig = {
		src: proj.source.js,
		dest: proj.build.js,
		name: {
			helpers: 'helpers.' + proj.tag + '.js',
			main: proj.tag + '.js',
			ie8: proj.tag + '-ie8.js',
		}
	};

	var scripts = {		
		helpers: [
			taskConfig.src + "vendor/modernizr.js",
			taskConfig.src + "vendor/viewport.js",
		],
		main: [
            taskConfig.src + "vendor/jquery-2.1.4.js",
            taskConfig.src + "counter.js"
        ],
        ie8: [
            taskConfig.src + "vendor/jquery-1.11.3.js",
            taskConfig.src + "counter.js"
        ]
	};

	return function () {
		gulp.task('uglify', function () {
		   	gulp.src( scripts.helpers )
		   		.pipe(gulpConcat( taskConfig.name.helpers ))
		   		.pipe(gulpIf( proj.production, uglify()) )
		      	.pipe(gulp.dest( taskConfig.dest ));

	      	gulp.src( scripts.main )
	      		.pipe(sourcemaps.init())
		   			.pipe(gulpConcat( taskConfig.name.main ))
		   			.pipe(gulpIf( proj.production, uglify()) )
	   			.pipe(sourcemaps.write())
		      	.pipe(gulp.dest( taskConfig.dest ));

	      	gulp.src( scripts.ie8 )
	      		.pipe(sourcemaps.init())
			   		.pipe(gulpConcat( taskConfig.name.ie8 ))
			   		.pipe(gulpIf( proj.production, uglify()) )
		   		.pipe(sourcemaps.write())
		      	.pipe(gulp.dest( taskConfig.dest ));
		});
	};
}
