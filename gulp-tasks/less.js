module.exports = function ( gulp, plugins ) {
	
	var gulp = require('gulp'),
	 	gulpIf = require('gulp-if'),
	 	sourcemaps = require('gulp-sourcemaps'),
		proj = require('../gulp-config'),
		less = require('gulp-less'),
		minifyCss = require('gulp-clean-css');
		

	var taskConfig = {
		path: proj.source.less + proj.master + ".less",
		pathFixed: proj.source.less + proj.master + "-fixed.less",
		dest: proj.build.css
	};

	return function () {
		gulp.task('less', function () {
		    gulp.src( taskConfig.path )
		    	.pipe(sourcemaps.init())
			    	.pipe(less().on('error', console.log))
			    	.pipe(gulpIf( proj.production, minifyCss()) )
		    	.pipe(sourcemaps.write())
		        .pipe(gulp.dest( taskConfig.dest ));

	         gulp.src( taskConfig.pathFixed )
		    	.pipe(sourcemaps.init())
			    	.pipe(less().on('error', console.log))
			    	.pipe(gulpIf( proj.production, minifyCss()) )
		    	.pipe(sourcemaps.write())
		        .pipe(gulp.dest( taskConfig.dest ));
		});
	};
}