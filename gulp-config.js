// Set Baseline
var config = {};

config.pkg = require('./package.json');
config.tag = config.pkg.name;
config.master = 'global';
config.production = true;

// Root Paths
config.source = "";
config.build = config.source + "build/";


// Specific Directories
config.source = {
  less: config.source + "less/",
  js: config.source + "js/"
};

config.build = {
  js: config.build + "js/",
  css: config.build + "css/"
};

module.exports = config;
